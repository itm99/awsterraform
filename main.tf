terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}
# Create the VPC for the resources
module "VPC" {
  source         = "./VPC"
  vpc_name       = var.vpc_name
  vpc_cidr_block = "10.0.0.0/16"
}
# Create the Security Groups for the VMs and ELB
module "SecurityGroups" {
  depends_on = [
    module.VPC
  ]
  source         = "./SecurityGroups"
  vpc_id         = module.VPC.vpc_id
  vpc_cidr_block = "10.0.0.0/16"
}
# Create the first VM
module "VM1" {
  depends_on = [
    module.VPC,
    module.SecurityGroups
  ]
  source                   = "./EC2"
  vpc_id                   = module.VPC.vpc_id
  ami                      = var.ami
  instance_type            = var.instance_type
  security_group_id        = module.SecurityGroups.vm_security_group_id
  vpc_cidr_block           = "10.0.0.0/16"
  subnet_cidr_block        = "10.0.0.0/24"
  subnet_availability_zone = "${var.region}a"
  subnet_name              = "VM-Subnet"
  vm_name                  = "vm"
  key_name                 = var.key_name
}
# Create the second VM
module "VM2" {
  depends_on = [
    module.VPC,
    module.SecurityGroups
  ]
  source                   = "./EC2"
  vpc_id                   = module.VPC.vpc_id
  ami                      = var.ami
  instance_type            = var.instance_type
  security_group_id        = module.SecurityGroups.vm_security_group_id
  vpc_cidr_block           = "10.0.0.0/16"
  subnet_cidr_block        = "10.0.1.0/24"
  subnet_availability_zone = "${var.region}b"
  subnet_name              = "VM2-Subnet"
  vm_name                  = "vm2"
  key_name                 = var.key_name
}
# Create the third VM
module "VM3" {
  depends_on = [
    module.VPC,
    module.SecurityGroups
  ]
  source                   = "./EC2"
  vpc_id                   = module.VPC.vpc_id
  ami                      = var.ami
  instance_type            = var.instance_type
  security_group_id        = module.SecurityGroups.vm_security_group_id
  vpc_cidr_block           = "10.0.0.0/16"
  subnet_cidr_block        = "10.0.2.0/24"
  subnet_availability_zone = "${var.region}c"
  subnet_name              = "VM3-Subnet"
  vm_name                  = "vm3"
  key_name                 = var.key_name
}
# Create the Load Balancer
module "ELB" {
  source            = "./ELB"
  elb_name          = "server-load-balancer"
  vpc_id            = module.VPC.vpc_id
  subnet_list       = [module.VM1.subnet_id, module.VM2.subnet_id, module.VM3.subnet_id]
  security_group_id = module.SecurityGroups.elb_security_group_id
  instance_list     = [module.VM1.vm_id, module.VM2.vm_id, module.VM3.vm_id]
}
