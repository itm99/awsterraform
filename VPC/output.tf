# Ouput the VPC id for use by other modules
output "vpc_id" {
  value     = aws_vpc.vpc.id
  sensitive = true
}
