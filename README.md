# AWSTerraform

## Steps To Deploy
1. Clone the Repository
2. Generate Secret/Access Keys using the AWS IAM console and write them down
3. Generate a Key Pair (Default name is 'vm-key') and download the public key. Move the public key to the same folder the project is in
4. Change the read+write permissions of the key`chmod 600 awsterraform/path/to/key`
5. Run `terraform init` 
6. **Optional** Run `terraform plan` and enter your access and secret keys when prompted (If you want to review the changes)
7. Run `terraform apply` and enter your access and secret keys when prompted to create the resources
8. Run `terraform destroy` to remove all created resources

## Improvements
- Introduce HTTPS for secure connections. Possibly using CloudFront
- Use an ASG to allow for automatic scaling based on demand

