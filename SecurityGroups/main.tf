# Create Security Group for the EC2 instances
resource "aws_security_group" "vm_security_group" {
  name        = "vm_sg"
  description = "Security Group for VMs"
  vpc_id      = var.vpc_id
  # Allow HTTP access from the Load Balancer
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  # Allow SSH access from any host 
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Allow internet access for the instances
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# Create Security Group for the Load Balancer
resource "aws_security_group" "elb_security_group" {
  name        = "elb_sg"
  description = "Security Group for the ELB"
  vpc_id      = var.vpc_id

  # Allow HTTP access from any host
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Allow outband access to the health check and instance listener
  egress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.vm_security_group.id]
  }
}
