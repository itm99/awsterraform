# Ouput the VM Security Group id for use in the main file
output "vm_security_group_id" {
  value     = aws_security_group.vm_security_group.id
  sensitive = true
}
# Ouput the ELB Security Group id for use in the main file
output "elb_security_group_id" {
  value     = aws_security_group.elb_security_group.id
  sensitive = true
}
