# Create the load balancer
resource "aws_elb" "elb" {
  name            = var.elb_name
  security_groups = [var.security_group_id]
  subnets         = var.subnet_list
  instances       = var.instance_list
  # Listen on port 80 of the instances and forward
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  # Check for a response on port 80 of the instances
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:80/"
    interval            = 10
  }
}
