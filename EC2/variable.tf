variable "vpc_id" {
}
variable "ami" {
}
variable "instance_type" {
}
variable "vpc_cidr_block" {
}
variable "subnet_cidr_block" {
}
variable "subnet_availability_zone" {
}
variable "subnet_name" {
}
variable "vm_name" {
}
variable "key_name" {
}
variable "security_group_id" {
}
