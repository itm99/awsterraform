# Output the subnet id for the instance for use in the main file
output "subnet_id" {
  value     = aws_subnet.subnet.id
  sensitive = true
}
# Output the instance id for the instance for use in the main file
output "vm_id" {
  value     = aws_instance.instance.id
  sensitive = true
}
