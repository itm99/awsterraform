# Create subnet for the instance
resource "aws_subnet" "subnet" {
  vpc_id            = var.vpc_id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.subnet_availability_zone

  tags = {
    Name = var.subnet_name
  }
}

# Create EC2 instance
resource "aws_instance" "instance" {
  ami                         = var.ami
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.subnet.id
  vpc_security_group_ids      = [var.security_group_id]
  associate_public_ip_address = true
  key_name                    = var.key_name
  provisioner "remote-exec" {
    connection {
      user        = "ubuntu"
      host        = self.public_ip
      type        = "ssh"
      private_key = file("${var.key_name}.pem")
    }
    inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install nginx",
      "sudo service nginx start",
    ]
  }
  tags = {
    Name = var.vm_name
  }
}
