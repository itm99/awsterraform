# Account Variables
variable "region" {
  default     = "eu-west-2"
  type        = string
  description = "Region for resources to be deployed"
}

variable "access_key" {
  type        = string
  description = "Access Key for AWS account"
}

variable "secret_key" {
  type        = string
  description = "Secret Key for AWS account"
}

# VPC Variables

variable "vpc_name" {
  default     = "vpc"
  type        = string
  description = "Name for VPC"
}

# EC2 Variables
variable "ami" {
  default     = "ami-0e312244e15c975b0"
  type        = string
  description = "AMI image for instances (Use a Debian/Ubuntu based image)"
}
variable "instance_type" {
  default     = "t2.nano"
  type        = string
  description = "Instance type for instances"
}
variable "key_name" {
  default     = "vm-key"
  type        = string
  description = "Name of the SSH key"
}

# ELB Variables
variable "elb_name" {
  default     = "server-load-balancer"
  type        = string
  description = "Name for the ELB instance"
}
